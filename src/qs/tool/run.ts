module qs {
    /**
     * 运行代码
     * @code 代码
     * @mainClassPath 主类的路径
     * @globalObject 代码中的对象所在的全局对象，可以传window或global等
     * @autoNew 是否自动实例化，如果为false，则返回一个类，否则直接返回实例好的对象，一般启动类需要传递参数的时候才需要为false
     */
    export function run(code: string, mainClassPath: string, globalObject: any, autoNew: boolean = true): any {
        var runtime = new qs.Runtime(globalObject);//运行环境
        runtime.regedit(code);
        var MainClass = runtime.run(mainClassPath);
        if (autoNew) {
            return new MainClass();
        }
        else {
            return MainClass;
        }
    }
}